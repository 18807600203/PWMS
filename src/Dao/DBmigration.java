package Dao;

import java.io.IOException;
import java.util.Properties;

import org.flywaydb.core.Flyway;

public class DBmigration {
	
    private static Properties config = new Properties(); 
    // 读取数据库配置参数  
    static {  
        try {  
            config.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("application.db.properties")); 
        } catch (IOException e) {  
            e.printStackTrace();  
        }  
    }  
      
    // 执行数据库版本升级  
    public static void migration() {  
        Flyway flyway = new Flyway();  
        flyway.setDataSource(config.getProperty("db.mysql.url"), config.getProperty("db.mysql.username"), config.getProperty("db.mysql.password"));    
        flyway.migrate();  
    }  
    
	public static void main(String[] args) {
    	migration();
    }
}
