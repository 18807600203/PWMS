--	语法:alter table 表名 add constraint FK_ID foreign key(你的外键字段名) REFERENCES 外表表名(对应的表的主键字段名);


--	employee & department table FK_dp_id
--	职工明细表 和 部门目录表 的外键  部门代号 dp_id
-------------------------------------------------------------------------------
alter table employee add constraint FK_dp_id foreign key(dp_id) references department(dp_id) on update cascade on delete restrict;


--	wageprice & pieceworkitem table FK_pwi_id
--	计件项目表 和 工资价格表 的外键  计件项目表代号  pwi_id
-------------------------------------------------------------------------------
alter table wageprice add constraint FK_pwi_id foreign key(pwi_id) references pieceworkitem(pwi_id) on update cascade on delete restrict;


--	pieceworkwage & employee table FK_e_id
--	计件工资表 和 职工明细表 的外键  职工编号 e_id
-------------------------------------------------------------------------------
alter table pieceworkwage add constraint FK_e_id foreign key(e_id) references employee(e_id) on update cascade on delete restrict;


--	usr & usrcategory table FK_usrc_id
--	用户表 和 权限类别表 的外键  权限编号 usrc_id
-------------------------------------------------------------------------------
alter table usr add constraint FK_usrc_id foreign key(usrc_id) references usrcategory(usrc_id) on update cascade on delete restrict;