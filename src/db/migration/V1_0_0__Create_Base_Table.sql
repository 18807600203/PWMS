--  department table
--  部门目录表
-------------------------------------------------------------------------------
create table department (
	dp_id  varchar(10) not null,
	dp_name varchar(50) not null,
	dp_detail bit not null,
	dp_order int not null,
	primary key( dp_id ),
	index index_id ( dp_id(10) )
);


--  pieceworkitem table
--  计件项目表
-------------------------------------------------------------------------------
create table pieceworkitem (
	pwi_id varchar(10) not null,
	pwi_name varchar(50) not null,
	pwi_detail bit not null,
	pwi_order int not null,
	primary key( pwi_id ),
	index index_id ( pwi_id(10) )
);


--  wageprice table
--  工资价格表
-------------------------------------------------------------------------------
create table wageprice ( 
	pwi_id varchar(10) not null,
	wp_item varchar(20) not null,
	wp_item_unit varchar(10) not null,
	price decimal(15,5) not null,
	primary key( pwi_id ),
	index index_id ( pwi_id(10) )
);


--  employee table
--	职工明细表
-------------------------------------------------------------------------------
create table employee (	
	e_id varchar(10) not null,
	e_name varchar(10) not null,	
	e_sex bit not null,
	dp_id  varchar(10) not null,
	primary key( e_id ),
	index index_id( e_id(10) )	
);


--  pieceworkwage table
--	计件工资表
-------------------------------------------------------------------------------
create table pieceworkwage (
	id bigint unsigned  not null AUTO_INCREMENT, 
	e_id varchar(10) not null,
	e_name varchar(20) not null,
	dp_name varchar(10) not null,
	mo_id varchar(20) not null,
	pwi_id varchar(10) not null,
	sworkingtime datetime not null,
	eworkingtime datetime not null,
	wp_item varchar(20) not null,
	workload int not null,
	price decimal(15,5) not null,
	sum_price decimal(15,5) not null,
	inserttime datetime not null,
	primary key( id ),
	index e_pwi_mo_id( e_id(10),pwi_id(10),mo_id(10) )	
);


--  usr table
--	用户表
-------------------------------------------------------------------------------
create table usr (
	usr_id varchar(10) not null,
	usr_psw varchar(20) not null,
	rem varchar(50),
	usrc_id tinyint unsigned not null,
	primary key( usr_id,usrc_id ),
	index index_id( usr_id(10) )
);


--  usr table
--	用户类别表
-------------------------------------------------------------------------------
create table usrcategory (
	usrc_id tinyint unsigned  not null,
	usrc_name varchar(10) not null,
	primary key( usrc_id )
);